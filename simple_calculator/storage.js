const localHistory = "HISTORY";

function putHistoryData(data) {
  if (typeof Storage !== "undefined") {
    let historyData = null;
    if (localStorage.getItem(localHistory) == null) {
      historyData = "";
    } else {
      historyData = JSON.parse(localHistory);
    }
    historyData.unshift(data);
    if (historyData.lenght > 5) {
      historyData.pop();
    }
    localStorage.setItem(localHistory, JSON.stringify(historyData));
  }
}
function showHistory() {
  if (typeof Storage !== "undefined") {
    return JSON.parse(localStorage.getItem(localHistory)) || "";
  } else {
    return "";
  }
}
function renderHistory() {
  const historyData = showHistory();
  let historyList = document.querySelector("#historyList");
  historyList.innerHTML = "";
  for (let history of historyData) {
    let row = document.createElement("tr");
    row.innerHTML = "<td>" + history.firstNumber + "</td>";
    row.innerHTML = "<td>" + history.operator + "</td>";
    row.innerHTML = "<td>" + history.secondNumber + "</td>";
    row.innerHTML = "<td>" + history.result + "</td>";
    historyList.appendChild(row);
  }
}
renderHistory();
